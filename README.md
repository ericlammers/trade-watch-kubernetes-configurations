# Description
This project contains the configuration files to set up the Trade Watch Kubernetes cluster.

## Installation
1. Build the images  
    a. To deploy the pods you first need to build each of the docker images. This must be done from within each of the projects repositories using the provided Docker file to build the image.  
2. Update the configuration files
    a. Update the configuration files to point to newly the correct image
3. Review environment variables  
    a. Update environment variables as needed
4. Deploy the pods   
    a. Use `kubectl create -f {file-name}` to deploy the pod
5. Expose the pods using services

## IBM Cloud 
### Steps to deploy (pods, services, deployments) to IBM Cloud
1. Log into ibm cloud through the cli `ibmcloud login`
2. Check cluster status `ibmcloud ks workers --cluster mycluster`
3. Get cluster configuration details `ibmcloud ks cluster-config --cluster mycluster`
4. Set KUBECONFIG to point to the downloaded config, command to do this will be given by output of step 3 (example only: `export KUBECONFIG=/Users/<user_name>/.bluemix/plugins/container-service/clusters/pr_firm_cluster/kube-config-prod-par02-pr_firm_cluster.yml
`)
5. Check that you can now access the cluster (one way to do this is to list out the podes `kubectl get pods`)
6. You can now proceed with deploying to the cluster using the regular kubectl commands (for example `kubectl create -f service.yml`)

(See the article [Tutorial: Creating Kubernetes clusters](https://console.bluemix.net/docs/containers/cs_tutorials.html#cs_cluster_tutorial) for more details)

### Making a pod, service or deployment externally accessible
See the article [Tutorial: Deploying apps into Kubernetes clusters](https://console.bluemix.net/docs/containers/cs_tutorials_apps.html#cs_apps_tutorial)

## Notes 
- A helpful kubernetes tutorial can be found here: [Learn Kubernetes in Under 3 Hours: A Detailed Guide to Orchestrating Containers](https://medium.freecodecamp.org/learn-kubernetes-in-under-3-hours-a-detailed-guide-to-orchestrating-containers-114ff420e882)


